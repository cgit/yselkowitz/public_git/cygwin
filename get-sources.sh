v=$(grep '^Version:' cygwin.spec | sed -e 's/.* //')
r=1
tarsrc=cygwin-$v-$r-src.tar.xz
url=https://sourceware.org/pub/cygwin/x86/release/cygwin/${tarsrc}

curl --fail --remote-time --location --output ${tarsrc} $url
tar axf ${tarsrc} cygwin-$v-$r.src/newlib-cygwin-$v.tar.bz2 \
  && mv cygwin-$v-$r.src/newlib-cygwin-$v.tar.bz2 . \
  && rmdir cygwin-$v-$r.src && rm -f ${tarsrc}
